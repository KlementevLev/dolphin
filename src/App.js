import logo from "./logo.svg";
import "./App.css";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

function App() {
  const [text, setText] = useState();
  const [table, setTable] = useState([]);
  function getSubstring(str, start) {
    const result = str.match(new RegExp(start + "([\\s\\S]*?)" + " "));
    if (result) {
      return result[1];
    } else {
      const result = str.match(new RegExp(start + "(.*)"));
      if (result) {
        return result[1];
      }
    }
  }
  const parse = () => {
    let emailParse = getSubstring(text, "Email: ");
    if (emailParse) {
      emailParse = emailParse.split(":");
    }
    let fbParse = getSubstring(text, "Fb: ");
    if (fbParse) {
      fbParse = fbParse.split(":");
    }
    const token = getSubstring(text, "Token: ");
    setTable([
      ...table,
      {
        emailLogin: emailParse ? emailParse[0] : null,
        emailPass: emailParse ? emailParse[1] : null,
        fbLogin: fbParse ? fbParse[0] : null,
        fbPass: fbParse ? fbParse[1] : null,
        token: token,
      },
    ]);
  };

  return (
    <div className="App">
      <div className={"App-header"}>
        <img src={logo} alt="Logo" />
        <Button onClick={parse} variant="contained">
          Парсить
        </Button>
      </div>
      <div className={"container"}>
        <TextField
          label="Введите или вставьте данные из шопов"
          id="outlined-multiline-static"
          multiline
          rows={8}
          fullWidth
          value={text}
          onChange={(event) => {
            setText(event.target.value);
          }}
        />
        <div>
          Пример - Email: email@rambler.ru:passEmail Fb: fb@mail.ru:passFB
          Token: 3Gdh564j
        </div>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Email Login</TableCell>
                <TableCell>Email password</TableCell>
                <TableCell>Facebook login</TableCell>
                <TableCell>Facebook password</TableCell>
                <TableCell>Facebook token</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {table?.map((row) => (
                <TableRow
                  key={row.name}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.emailLogin}
                  </TableCell>
                  <TableCell>{row.emailPass}</TableCell>
                  <TableCell>{row.fbLogin}</TableCell>
                  <TableCell>{row.fbPass}</TableCell>
                  <TableCell>{row.token}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
}

export default App;
